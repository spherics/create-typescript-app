# Spherics Create Typescript App

Create new TypeScript apps with consistent `tsconfig.json`, ESLint and Prettier config.

## Usage

`yarn create @sphericsio/typescript-app <your-app-name>`

or 

`npx @sphericsio/create-typescript-app <your-app-name>`

## Private yarn/npm registry setup

Add the following line to your `~/.npmrc` file:

`@sphericsio:registry = <private registry url>`

Then login to the registry to set your credentials:

`npm login --registry=<private registry url>`
