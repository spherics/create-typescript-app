import {Options} from 'prettier';

const config: Options = {
    semi: true,
    trailingComma: "all",
    singleQuote: true,
    printWidth: 100,
    tabWidth: 4,
    bracketSpacing: false,
};

export = config;
