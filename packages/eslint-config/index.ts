import {Linter} from 'eslint';

const config: Linter.Config = {
    parser: "@typescript-eslint/parser", // Specifies the ESLint parser
    plugins: ['@typescript-eslint'],
    extends: [
      "eslint:recommended",
      "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
      "plugin:prettier/recommended", // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    ],
    parserOptions: {
      ecmaVersion: 2017, // Allows for the parsing of modern ECMAScript features
      sourceType: "module", // Allows for the use of imports
    },
    env: {
      'shared-node-browser': true,
      es2017: true,
    },
    rules: {
      'no-console': ['error', {allow: ['warn', 'error']}],
      '@typescript-eslint/explicit-function-return-type': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      'no-unused-vars': 'off',
      '@typescript-eslint/no-unused-vars': ['warn']
    },
};

export = config;