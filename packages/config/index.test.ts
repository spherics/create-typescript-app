import {IsExact, assert} from 'conditional-type-checks';
import {
    Configuration,
    PartialConfiguration,
    EnvVarConfigurationOverrides,
    parseConfig,
    booleanEnvParser,
    integerEnvParser,
    replaceMe,
    stringArrayEnvParser,
} from './index';

const defaultConfig = {
    testString: 'testString',
    testNumber: 1234,
    testBoolean: true,
    testNested: {
        value: 'nestedValue',
        value2: 'nestedValue2',
    },
    testArray: ['some', 'values'],
};
type Config = Configuration<typeof defaultConfig>;

describe('parseConfig', () => {
    const config = parseConfig(defaultConfig);

    test('types', () => {
        assert<IsExact<typeof config, Config>>(true);
    });

    test('parse string', () => {
        expect(config.testString).toBe('testString');
    });

    test('parse number', () => {
        expect(config.testNumber).toBe(1234);
    });

    test('parse boolean', () => {
        expect(config.testBoolean).toBe(true);
    });

    test('parse nested', () => {
        expect(config.testNested.value).toBe('nestedValue');
    });

    test('parse array', () => {
        expect(config.testArray).toEqual(['some', 'values']);
    });
});

describe('parseConfig with local overrides', () => {
    const localConfig: PartialConfiguration<Config> = {
        testString: 'localTestString',
        testNested: {
            value2: 'localNestedValue2',
        },
        testArray: ['override', 'values'],
    };

    const config = parseConfig(defaultConfig, localConfig);

    test('value is overriden', () => {
        expect(config.testString).toBe('localTestString');
    });

    test('nested values are merged', () => {
        expect(config.testNested.value).toBe('nestedValue');
        expect(config.testNested.value2).toBe('localNestedValue2');
    });
});

describe('parseConfig with env value overrides', () => {
    const environment = {
        TEST_STRING: 'fromEnvString',
        TEST_NUMBER: '42',
        TEST_NESTED_VALUE: 'test',
        TEST_ARRAY: 'env,values',
    };

    const envOverrides: EnvVarConfigurationOverrides<Config> = {
        testString: 'TEST_STRING',
        testNumber: (env) => {
            return parseInt(env.TEST_NUMBER as string, 10);
        },
        testNested: {
            value: (env) => {
                return env.TEST_NESTED_VALUE === 'test' ? 'test' : 'noMatch';
            },
        },
        testArray: (env) => {
            if (env.TEST_ARRAY == null) {
                return;
            }
            return env.TEST_ARRAY.split(',');
        },
    };

    const config = parseConfig(defaultConfig, undefined, envOverrides, environment);

    test('from env value', () => {
        expect(config.testString).toBe('fromEnvString');
    });

    test('from function', () => {
        expect(config.testNumber).toBe(42);
        type EnvVarParser = Extract<
            typeof envOverrides.testNumber,
            (env: NodeJS.ProcessEnv) => number | void
        >;
        assert<IsExact<ReturnType<EnvVarParser>, number | void>>(true);
    });

    test('from nested', () => {
        expect(config.testNested.value).toBe('test');
    });
});

test('env value parser when env value is missing', () => {
    const envOverrides: EnvVarConfigurationOverrides<Config> = {
        testString: 'TEST_STRING',
    };
    const config = parseConfig(defaultConfig, undefined, envOverrides, {});

    expect(config.testString).toBe('testString');
});

test('env value parser function can return null', () => {
    const envOverrides: EnvVarConfigurationOverrides<Config> = {
        testString: () => {
            //empty function returns undefined
        },
    };
    const config = parseConfig(defaultConfig, undefined, envOverrides, {});

    expect(config.testString).toBe('testString');
});

describe('provided boolean env value parser', () => {
    const envOverrides: EnvVarConfigurationOverrides<Config> = {
        testBoolean: booleanEnvParser('TEST_BOOLEAN'),
    };

    test('true value', () => {
        const config = parseConfig(defaultConfig, undefined, envOverrides, {TEST_BOOLEAN: 'true'});
        expect(config.testBoolean).toBe(true);
    });

    test('t value', () => {
        const config = parseConfig(defaultConfig, undefined, envOverrides, {TEST_BOOLEAN: 't'});
        expect(config.testBoolean).toBe(true);
    });

    test('1 value', () => {
        const config = parseConfig(defaultConfig, undefined, envOverrides, {TEST_BOOLEAN: '1'});
        expect(config.testBoolean).toBe(true);
    });

    test('false value', () => {
        const config = parseConfig(defaultConfig, undefined, envOverrides, {TEST_BOOLEAN: 'false'});
        expect(config.testBoolean).toBe(false);
    });

    test('f value', () => {
        const config = parseConfig(defaultConfig, undefined, envOverrides, {TEST_BOOLEAN: 'f'});
        expect(config.testBoolean).toBe(false);
    });

    test('0 value', () => {
        const config = parseConfig(defaultConfig, undefined, envOverrides, {TEST_BOOLEAN: '0'});
        expect(config.testBoolean).toBe(false);
    });

    test('unsupported value', () => {
        expect(() =>
            parseConfig(defaultConfig, undefined, envOverrides, {TEST_BOOLEAN: 'unsupported'}),
        ).toThrow(
            'Error handling env var at key testBoolean: Error: invalid env value unsupported for key TEST_BOOLEAN',
        );
    });
});

describe('provided integer env value parser', () => {
    const envOverrides: EnvVarConfigurationOverrides<Config> = {
        testNumber: integerEnvParser('TEST_NUMBER'),
    };

    test('number value', () => {
        const config = parseConfig(defaultConfig, undefined, envOverrides, {TEST_NUMBER: '42'});
        expect(config.testNumber).toBe(42);
    });

    test('unsupported value', () => {
        expect(() =>
            parseConfig(defaultConfig, undefined, envOverrides, {TEST_NUMBER: 'unsupported'}),
        ).toThrow(
            'Error handling env var at key testNumber: Error: invalid number unsupported for key TEST_NUMBER',
        );
    });
});

describe('provided string array env value parser', () => {
    const envOverrides: EnvVarConfigurationOverrides<Config> = {
        testArray: stringArrayEnvParser('TEST_ARRAY'),
    };

    test('comma seperated values', () => {
        const config = parseConfig(defaultConfig, undefined, envOverrides, {
            TEST_ARRAY: 'test,comma,values',
        });
        expect(config.testArray).toEqual(['test', 'comma', 'values']);
    });
});

describe('empty values in default config', () => {
    const defaultConfig = {
        testString: replaceMe<string>(),
    };
    type Config = Configuration<typeof defaultConfig>;

    test('when replaced by local config', () => {
        const local: PartialConfiguration<Config> = {
            testString: 'replaced',
        };

        const config = parseConfig(defaultConfig, local);

        assert<IsExact<typeof config.testString, string>>(true);
        expect(config.testString).toBe('replaced');
    });

    test('when replaced by env config', () => {
        const envOverrides: EnvVarConfigurationOverrides<Config> = {
            testString: 'TEST_STRING',
        };

        const config = parseConfig(defaultConfig, undefined, envOverrides, {
            TEST_STRING: 'replaced',
        });

        assert<IsExact<typeof config.testString, string>>(true);
        expect(config.testString).toBe('replaced');
    });

    test('when not replaced', () => {
        const local: PartialConfiguration<Config> = {};
        expect(() => {
            parseConfig(defaultConfig, local);
        }).toThrow('Missing value for key testString');
    });

    test('nested when not replaced', () => {
        const defaultConfig = {
            nested: {
                value: replaceMe<string>(),
            },
        };
        type Config = Configuration<typeof defaultConfig>;
        const local: PartialConfiguration<Config> = {};
        expect(() => {
            parseConfig(defaultConfig, local);
        }).toThrow('Missing value for key nested.value');
    });
});

test('config can be a structural type', () => {
    type SubConfig = {
        key: string;
    };

    const subConfig: SubConfig = {
        key: 'subKey',
    };

    const defaultConfig = {
        subConfig,
    };
    type Config = Configuration<typeof defaultConfig>;
    const config = parseConfig(defaultConfig);
    assert<IsExact<typeof config, Config>>(true);
    assert<IsExact<typeof config.subConfig.key, string>>(true);
});

describe('config can have typed enums', () => {
    type SubConfig = {
        stringKey: 'key1' | 'key2';
        numberKey: 80 | 443;
    };

    const defaultConfig: SubConfig = {
        stringKey: 'key2',
        numberKey: 80,
    };
    type Config = Configuration<typeof defaultConfig>;
    const config = parseConfig(defaultConfig);

    test('typeof config', () => {
        assert<IsExact<typeof config, Config>>(true);
    });

    test('typeof string key', () => {
        assert<IsExact<typeof config.stringKey, 'key1' | 'key2'>>(true);
        assert<IsExact<PartialConfiguration<Config>['stringKey'], 'key1' | 'key2' | undefined>>(
            true,
        );

        type EnvVarParser = Extract<
            EnvVarConfigurationOverrides<Config>['stringKey'],
            (env: NodeJS.ProcessEnv) => string | void
        >;
        assert<IsExact<ReturnType<EnvVarParser>, 'key1' | 'key2' | void>>(true);
    });

    test('typeof number key', () => {
        assert<IsExact<typeof config.numberKey, 80 | 443>>(true);
        assert<IsExact<PartialConfiguration<Config>['numberKey'], 80 | 443 | undefined>>(true);

        type EnvVarParser = Extract<
            EnvVarConfigurationOverrides<Config>['numberKey'],
            (env: NodeJS.ProcessEnv) => number | void
        >;
        assert<IsExact<ReturnType<EnvVarParser>, 80 | 443 | void>>(true);
    });
});

test('config can be a structural type with optional keys', () => {
    type ConfigShape = {
        required: string;
        optional?: string;
    };

    const defaultConfig: ConfigShape = {
        required: 'required',
        optional: 'optional',
    };
    type Config = Configuration<typeof defaultConfig>;
    const config = parseConfig(defaultConfig);
    assert<IsExact<typeof config, Config>>(true);
    assert<IsExact<typeof config.optional, string | undefined>>(true);
});

test('local config is not enforced to override optional config', () => {
    type ConfigShape = {
        required: string;
        optional?: string;
    };

    const defaultConfig: ConfigShape = {
        required: 'required',
    };
    type Config = Configuration<typeof defaultConfig>;

    const localConfig: PartialConfiguration<Config> = {};

    const config = parseConfig(defaultConfig, localConfig);
    assert<IsExact<typeof config, Config>>(true);
    assert<IsExact<typeof config.optional, string | undefined>>(true);
});

test('env config parsed via function is not enforced to return a value for optional config', () => {
    type ConfigShape = {
        required: string;
        optional?: string;
    };

    const defaultConfig: ConfigShape = {
        required: 'required',
    };
    type Config = Configuration<typeof defaultConfig>;

    const fromEnv: EnvVarConfigurationOverrides<Config> = {
        optional: () => {
            return undefined;
        },
    };

    const config = parseConfig(defaultConfig, undefined, fromEnv);
    assert<IsExact<typeof config, Config>>(true);
    assert<IsExact<typeof config.optional, string | undefined>>(true);
});

test('nested optional config is merged', () => {
    type ConfigShape = {
        optional?: {
            nested: string;
        };
    };

    const defaultConfig: ConfigShape = {};
    type Config = Configuration<typeof defaultConfig>;

    const localConfig: PartialConfiguration<Config> = {
        optional: {
            nested: 'nested',
        },
    };

    const config = parseConfig(defaultConfig, localConfig);
    expect(config.optional).toMatchObject({nested: 'nested'});
});
