type PrimitiveValueTypes = string | number | boolean | undefined;
type ConfigPrimitiveValueTypes = PrimitiveValueTypes | PrimitiveValueTypes[];
type MustBeReplaced<T extends ConfigPrimitiveValueTypes> = {__mustBeReplaced: true; __type: T};
type ConfigValue<T> = ConfigPrimitiveValueTypes | ConfigEntry<T> | ConfigPrimitiveValueTypes[];

interface ConfigEntry<T = ConfigPrimitiveValueTypes> {
    [key: string]: ConfigValue<T>;
}

export type Configuration<T> = T extends MustBeReplaced<infer M>
    ? M
    : T extends ConfigPrimitiveValueTypes
    ? T
    : T extends ConfigPrimitiveValueTypes[]
    ? T
    : T extends Partial<ConfigEntry>
    ? {[K in keyof T]: Configuration<T[K]>}
    : never;

export type PartialConfiguration<T> = T extends ConfigPrimitiveValueTypes
    ? Partial<T>
    : T extends ConfigPrimitiveValueTypes[]
    ? Partial<T>
    : T extends MustBeReplaced<infer M>
    ? M
    : T extends ConfigEntry
    ? {[K in keyof T]?: PartialConfiguration<T[K]>}
    : never;

type EnvVarParser<T> = (env: NodeJS.ProcessEnv) => T | void;
type FromEnvVarValue<T extends ConfigPrimitiveValueTypes> = string | EnvVarParser<T>;
export type EnvVarConfigurationOverrides<T> = T extends undefined
    ? FromEnvVarValue<T | undefined>
    : T extends boolean
    ? FromEnvVarValue<boolean>
    : T extends ConfigPrimitiveValueTypes
    ? FromEnvVarValue<T>
    : T extends MustBeReplaced<infer M>
    ? FromEnvVarValue<M>
    : T extends Partial<ConfigEntry>
    ? {[K in keyof T]?: EnvVarConfigurationOverrides<T[K]>}
    : never;

function envOverrideValues<T>(
    config?: EnvVarConfigurationOverrides<T>,
    path: Array<string> = [],
    environment: NodeJS.ProcessEnv = process.env,
): Record<string, any> {
    if (config == null) {
        return {};
    }

    return Object.entries(config).reduce((overrides, [key, value]) => {
        let envValue;
        if (typeof value === 'function') {
            try {
                envValue = value(environment);
            } catch (err) {
                throw new Error(
                    `Error handling env var at key ${path.concat(key).join('.')}: ${err}`,
                );
            }
        } else if (typeof value === 'string') {
            envValue = environment[value];
        } else {
            envValue = envOverrideValues(value, path.concat(key), environment);
        }

        if (envValue == null) {
            return overrides;
        }

        return {
            ...overrides,
            [key]: envValue,
        };
    }, {} as Record<string, any>);
}

const TRUE_ENV_VALUES = ['true', 't', '1'];
const FALSE_ENV_VALUES = ['false', 'f', '0'];
export function booleanEnvParser(envKey: string): EnvVarParser<boolean> {
    return (env) => {
        const envValue = env[envKey];
        if (envValue == null) {
            return;
        }

        const lowerEnvValue = envValue.toLowerCase();

        if (TRUE_ENV_VALUES.includes(lowerEnvValue)) {
            return true;
        }

        if (FALSE_ENV_VALUES.includes(lowerEnvValue)) {
            return false;
        }

        throw new Error(
            `invalid env value ${envValue} for key ${envKey}, acceptable values are ${TRUE_ENV_VALUES.concat(
                FALSE_ENV_VALUES,
            ).join(',')}`,
        );
    };
}

export function integerEnvParser(envKey: string): EnvVarParser<number> {
    return (env) => {
        const envValue = env[envKey];
        if (envValue == null) {
            return;
        }

        const numberEnvValue = parseInt(envValue, 10);
        if (isNaN(numberEnvValue)) {
            throw new Error(`invalid number ${envValue} for key ${envKey}`);
        }

        return numberEnvValue;
    };
}

export function stringArrayEnvParser(envKey: string): EnvVarParser<string[]> {
    return (env) => {
        const envValue = env[envKey];
        if (envValue == null) {
            return;
        }
        return envValue.split(',');
    };
}

const INTERNAL_REPLACE_ME = {};
export function replaceMe<T extends ConfigPrimitiveValueTypes>(): MustBeReplaced<T> {
    return INTERNAL_REPLACE_ME as MustBeReplaced<T>;
}

function checkMissingConfig<T>(defaultConfig: T, finalValue: any, path: Array<string> = []) {
    for (const [key, value] of Object.entries(defaultConfig)) {
        const resolvedValue = finalValue[key];
        if (resolvedValue === INTERNAL_REPLACE_ME) {
            throw new Error(`Missing value for key ${path.concat(key).join('.')}`);
        }

        if (typeof value === 'object') {
            checkMissingConfig(value, resolvedValue, path.concat(key));
        }
    }
}

function deepMerge<T extends Record<string, any>>(base: T, toMerge: Record<string, any>): T {
    return Object.keys(toMerge).reduce((final, key) => {
        let override = toMerge[key];
        if (typeof override === 'object' && !Array.isArray(override)) {
            override = deepMerge(base[key], override);
        }

        return {
            ...final,
            [key]: override,
        };
    }, base);
}

export function parseConfig<T extends ConfigEntry>(
    defaultConfig: T,
    localConfig?: PartialConfiguration<T>,
    envOverrides?: EnvVarConfigurationOverrides<T>,
    environment?: NodeJS.ProcessEnv,
): Configuration<T> {
    let finalConfig = defaultConfig;
    if (localConfig != null) {
        finalConfig = deepMerge(finalConfig, localConfig);
    }

    if (envOverrides != null) {
        finalConfig = deepMerge(
            finalConfig,
            envOverrideValues(envOverrides, undefined, environment),
        );
    }

    checkMissingConfig(defaultConfig, finalConfig);

    return finalConfig as Configuration<T>;
}
