import config from './config/index';
import logging from './logging';
const log = logging('app');

log.info({appName: config.appName}, 'App started');
