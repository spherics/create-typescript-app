import {parseConfig} from '@sphericsio/config';

import defaultConfig from './default';
let localConfig;
try {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    localConfig = require('./local').default;
} catch (ignored) {} //eslint-disable-line no-empty

export default parseConfig(defaultConfig, localConfig);
