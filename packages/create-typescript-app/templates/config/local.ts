import {PartialConfiguration} from '@sphericsio/config';
import {Config} from './default';

const config: PartialConfiguration<Config> = {};
export default config;
