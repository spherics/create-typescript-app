import logging from '@sphericsio/logging';
import config from './config/index';

export default logging(config.logging);
