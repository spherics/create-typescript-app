#!/usr/bin/env node

import path from 'path';
import fs from 'fs-extra';
import os from 'os';
import {program} from 'commander';
import spawn from 'cross-spawn';
import Handlebars from 'handlebars';

import tsconfigPackageJson from '@sphericsio/tsconfig/package.json';
import eslintConfigPackageJson from '@sphericsio/eslint-config/package.json';
import configPackageJson from '@sphericsio/config/package.json';
import loggingPackageJson from '@sphericsio/logging/package.json';

import {version} from './package.json';

function getDependenciesWithVersions(dependencies: {[dependency: string]: string}): Array<string> {
    return Object.keys(dependencies).map((key) => `${key}@${dependencies[key]}`);
}

function spawnCommand(command: string, args: Array<string>) {
    return new Promise<void>((resolve, reject) => {
        const child = spawn(command, args, {stdio: 'inherit'});
        child.on('close', (code) => {
            if (code !== 0) {
                reject({
                    command: `${command} ${args.join(' ')}`,
                });
                return;
            }
            resolve();
        });
    });
}

type CreateAppOptions = {
    lerna: boolean;
    org: string;
    path?: string;
};

// list of files that need to be renamed due to various issues
// eg .gitignore gets renamed to .npmignore when this package
// is published.
const renameFiles: {[key: string]: string} = {
    gitignore: '.gitignore',
};

function processTemplates(context: any, projectRoot: string, templatesRoot: string) {
    fs.readdirSync(templatesRoot).forEach((fileName) => {
        const filePath = path.join(templatesRoot, fileName);
        const file = fs.statSync(filePath);
        if (file.isDirectory()) {
            return processTemplates(context, path.join(projectRoot, fileName), filePath);
        }
        let fileContents;
        if (fileName.endsWith('.template')) {
            const template = Handlebars.compile(fs.readFileSync(filePath, 'utf8'));
            fileContents = template(context);
            fileName = fileName.replace(/\.template$/, '');
        } else {
            fileContents = fs.readFileSync(filePath);
        }

        fs.ensureDirSync(projectRoot);

        fs.writeFileSync(path.join(projectRoot, fileName), fileContents);
    });
}

async function installDependencies(root: string, dependencies: string[], dev: boolean) {
    if (dependencies.length === 0) {
        return;
    }
    console.log(`installing ${dev ? 'dev' : ''}dependencies`, dependencies);

    let args = ['--cwd', root, 'add'];
    if (dev) {
        args = args.concat('--dev');
    }
    args = args.concat(dependencies);
    await spawnCommand('yarn', args);
}

async function createApp(appName: string, opts: CreateAppOptions) {
    let root = path.resolve(appName);
    if (opts.path != null) {
        root = path.resolve(opts.path, appName);
    }
    console.log(`creating app ${appName} at ${root}`);
    fs.ensureDirSync(root);
    const appPackageName = `${opts.org}/${appName}`;
    const appPackageJson = {
        name: appPackageName,
        version: '1.0.0',
        private: true,
        prettier: '@sphericsio/eslint-config/lib/prettier',
        scripts: {
            build: 'tsc --build tsconfig.json',
            watch: 'yarn build -w',
            dev: 'ts-node-dev --respawn index.ts | pino-pretty -t -i hostname',
        },
    };

    fs.writeFileSync(
        path.join(root, 'package.json'),
        JSON.stringify(appPackageJson, null, 2) + os.EOL,
    );

    let devDependencies = getDependenciesWithVersions(tsconfigPackageJson.peerDependencies)
        .concat(getDependenciesWithVersions(eslintConfigPackageJson.peerDependencies))
        .concat(['pino-pretty', 'ts-node-dev']);

    let dependencies: string[] = [];

    const localPackageDependencies = {
        devDependencies: [tsconfigPackageJson.name, eslintConfigPackageJson.name],
        dependencies: [configPackageJson.name, loggingPackageJson.name],
    };
    if (!opts.lerna) {
        devDependencies = devDependencies.concat(localPackageDependencies.devDependencies);
        dependencies = dependencies.concat(localPackageDependencies.dependencies);
    }

    await installDependencies(root, devDependencies, true);
    await installDependencies(root, dependencies, false);

    if (opts.lerna) {
        for (const pkg of localPackageDependencies.devDependencies) {
            await spawnCommand('yarn', ['lerna', 'add', '--dev', pkg, `--scope=${appPackageName}`]);
        }

        for (const pkg of localPackageDependencies.dependencies) {
            await spawnCommand('yarn', ['lerna', 'add', pkg, `--scope=${appPackageName}`]);
        }
    }

    const templatesDir = path.join(__dirname, 'templates');
    processTemplates({appName}, root, templatesDir);

    Object.keys(renameFiles).forEach((file) => {
        if (fs.statSync(path.join(root, file))) {
            fs.renameSync(path.join(root, file), path.join(root, renameFiles[file]));
        }
    });
}

program
    .version(version)
    .arguments('<appName>')
    .option('--lerna', 'use lerna', false)
    .option('--org <org>', 'npm/yarn organisation', '@sphericsio')
    .option('-p, --path <path>', 'Output path')
    .action(createApp);

program
    .parseAsync(process.argv)
    .then(() => {
        console.log('Project created');
    })
    .catch((err) => {
        console.error('A problem occurred creating your project');
        console.error(err.stack);
    });
