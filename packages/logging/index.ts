import pino, {Level} from 'pino';

class WildcardMap<T> {
    private wildcards: {pattern: string; value: T}[];
    constructor(private map: Record<string, T>) {
        this.wildcards = Object.entries(map)
            .filter(([pattern]) => pattern.endsWith('*'))
            .map(([pattern, value]) => ({
                pattern: pattern.substr(0, pattern.length - 1),
                value,
            }))
            .sort((a, b) => b.pattern.length - a.pattern.length);
    }

    getValueForKey(key: string) {
        const exactMatch = this.map[key];
        if (exactMatch != null) {
            return exactMatch;
        }

        const matchFromWildcards = this.wildcards.find(({pattern}) => key.startsWith(pattern));
        if (matchFromWildcards != null) {
            return matchFromWildcards.value;
        }

        return undefined;
    }
}

export type LogLevelConfig = {
    defaultLevel: Level;
    levels?: {
        [key: string]: Level;
    };
};

type ReservedLogKeys = 'name' | 'level' | 'pid' | 'timestamp' | 'v';

type LogObj = Record<string, any> & Partial<Record<ReservedLogKeys, never>>;

type GuardedLogFn = {
    (message: string, ...args: any[]): void;
    (obj: LogObj, message: string, ...args: any[]): void;
};

type BetterTypedLoggerFunctions = {
    child: (...args: Parameters<pino.Logger['child']>) => Logger;
    fatal: GuardedLogFn;
    error: GuardedLogFn;
    warn: GuardedLogFn;
    info: GuardedLogFn;
    debug: GuardedLogFn;
    trace: GuardedLogFn;
};

export type Logger = Omit<pino.BaseLogger, keyof BetterTypedLoggerFunctions> &
    BetterTypedLoggerFunctions & {[key: string]: GuardedLogFn};

export type LogFactory = (name: string) => Logger;

export default function buildLogger(
    logLevelConfig?: LogLevelConfig,
    prettyPrint = false,
): LogFactory {
    const levelConfig: LogLevelConfig = {
        defaultLevel: 'debug',
        ...logLevelConfig,
    };

    const levels = new WildcardMap(levelConfig.levels ?? {});

    return function logFactory(name: string): Logger {
        let level = levels.getValueForKey(name);

        if (level == null) {
            level = levelConfig.defaultLevel;
        }

        return pino({
            name,
            level,
            formatters: {
                level: (level) => ({level}),
            },
            serializers: {
                err: pino.stdSerializers.err,
                req: pino.stdSerializers.req,
                res: pino.stdSerializers.res,
            },
            prettyPrint: prettyPrint
                ? {
                      translateTime: true,
                      ignore: 'hostname',
                  }
                : false,
        });
    };
}
